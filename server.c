#include <winsock2.h>

#include <windows.h>
#include <stdio.h>
#include <ws2tcpip.h>
#include <io.h>
#include <string.h>
#include <process.h>
#include <conio.h>
#include <zconf.h>
#include <time.h>
//Funktion um Winsock zu starten
int startWinsock(void) {
  WSADATA wsa;
  //Version 2.2 und wsa als Pointer
  return WSAStartup(MAKEWORD(2, 2), &wsa);
}

struct NeightbourInfo {
  int neightbourId;
  int port;
  time_t lastUpate;
  int cost;
} neightbourInfo;

struct SocketInfo {
  int id;
  int port;
  struct NeightbourInfo neightbours[5];
  int shouldExit;
} socketInfo;

void sendOutIDInfo(struct SocketInfo info, SOCKET s);
void startUDPSocket(struct SocketInfo info);
int main() {
  int myID;
  printf("ID: ");
  scanf("%d", &myID);
  printf("%d\n", myID);

  struct SocketInfo socket_info1[5];

  FILE *fPointer;
  char buf[MAXCHAR];
  char configZeileBeta[100][MAXCHAR];
  int i = 0;

  fPointer = fopen("ConfigFile", "r");
  if (fPointer == NULL) {
	printf("Could not open file %s", fPointer);
	return -1;
  }
  //Jede Zeile wird in ein Array gespeichert
  while (fgets(buf, MAXCHAR, fPointer) != NULL) {
	sprintf(configZeileBeta[i++], "%s", buf);
  }
  printf("%d\n", i);
  char configZeile[i][MAXCHAR];
  for (int j = 0; j < i; j++) {
	strcpy(configZeile[j], configZeileBeta[j]);
  }
  fclose(fPointer);
  size_t n = sizeof(configZeile) / sizeof(configZeile[0]);
  int j = 0;
  int h = 0;
  int innerCounter = 0;
  //Jede Zeile wird in ID, Port und Neighbour unterteilt
  while (j < n) {
	innerCounter = 0;
	i = 0;
	char *p = strtok(configZeile[j], ";");
	char *array[3];

	while (p != NULL) {
	  array[i++] = p;
	  p = strtok(NULL, ";");
	}

	while (innerCounter < strlen(array[2])) {
	  h = 0;
	  char *c = strtok(array[2], ",");
	  char neightbouhrs[5][25];

	  while (c != NULL) {
		//neightbouhrs[h++] = c;
		strcpy(neightbouhrs[h++], c);
		c = strtok(NULL, ",");;

	  }

	  socket_info1[j].id = atoi(array[0]);
	  socket_info1[j].port = atoi(array[1]);
	  socket_info1[j].shouldExit = 0;
	  for (int o = 0; o < 5; o++) {
		if (neightbouhrs[o] != NULL) {
		  socket_info1[j].neightbours[o].neightbourId = atoi(neightbouhrs[o]);
		  time_t start_t;
		  time(&start_t);
		  socket_info1[j].neightbours[o].lastUpate = start_t;
		  socket_info1[j].neightbours[o].cost = 0;
		}
	  }
	  for (int o = 0; o < 5; o++) {
		strcpy(neightbouhrs[o], "0");
	  }
	  innerCounter++;
	}
	j++;
  }
  size_t n2 = sizeof(socket_info1[0].neightbours) / sizeof(socket_info1[0].neightbours[0]);

  for (int o = 0; o < n; o++) {
	printf("Socketinfo an der Stelle %d:\tID: %d\tPort: %d\tneightbours: ",
		   o,
		   socket_info1[o].id,
		   socket_info1[o].port);
	for (int j = 0; j < n2; j++) {
	  printf("%d,", socket_info1[o].neightbours[j].neightbourId);
	  if (socket_info1[o].neightbours[j].neightbourId != 0) {
		socket_info1[o].neightbours[j].port = socket_info1[o].neightbours[j].neightbourId + 5000;
		socket_info1[o].neightbours[j].cost = 1;
	  } else {
		socket_info1[o].neightbours[j].port = 0;
	  }
	}
	printf("\n");
  }

  startUDPSocket(socket_info1[myID - 1]);

}

void startUDPSocket(struct SocketInfo mySocket_info) {
  int receivedNeighbourID = 0;
  int receivedNeighbourPORT = 0;
  int receivedNeighbourCOST = 0;

  time_t currentTime;

  printf("\nEintrag: %d\t", mySocket_info.id);
  printf("%d\t", mySocket_info.port);
  for (int kI = 0; kI < 5; kI++) {
	printf("%d,", mySocket_info.neightbours[kI]);
  }

  long rc;
  SOCKET s;
  char buf[256];
  SOCKADDR_IN addr;
  SOCKADDR_IN remoteAddr[5];
  int remoteAddrLen = sizeof(SOCKADDR_IN);
  rc = startWinsock();

  if (rc != 0) {
	printf("Fehler: startWinsock, fehler code: %d\n", rc);
	return;
  } else {
	printf("\nWinsock gestartet!\n");
  }

  //UDP Socket erstellen
  s = socket(AF_INET, SOCK_DGRAM, 0);
  if (s == INVALID_SOCKET) {
	printf("Fehler: Der Socket konnte nicht erstellt werden, fehler code: %d\n", WSAGetLastError());
	return;
  } else {
	printf("UDP Socket erstellt!\n");
  }

  addr.sin_family = AF_INET;
  addr.sin_port = htons(mySocket_info.port);
  addr.sin_addr.s_addr = ADDR_ANY;
  rc = bind(s, (SOCKADDR *)&addr, sizeof(SOCKADDR_IN));
  if (rc == SOCKET_ERROR) {
	printf("Fehler: bind, fehler code: %d\n", WSAGetLastError());
	return;
  } else {
	printf("Thread ID %d ,Socket an Port %d gebunden\n", mySocket_info.port, mySocket_info.port);
  }

  DWORD dw = 2;
  setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (char *)&dw, sizeof(dw));
  int keinAHnung = 1;
  while (keinAHnung) {
	sleep(5);
	sendOutIDInfo(mySocket_info, s);
	int recvCounter = 0;
	while (recvCounter < 5) {
	  rc = recvfrom(s, buf, 256, 0, (SOCKADDR *)&remoteAddr, &remoteAddrLen);
	  if (rc == SOCKET_ERROR) {
		if (WSAGetLastError() != 10060) {
		  time(&currentTime);
		  for (int i = 0; i < 5; i++) {
			if (difftime(currentTime, mySocket_info.neightbours[i].lastUpate) >= 30) {
			  mySocket_info.neightbours[i].neightbourId = 0;
			  mySocket_info.neightbours[i].lastUpate = 0;
			  mySocket_info.neightbours[i].port = 0;
			  mySocket_info.neightbours[i].cost = 0;
			}
		  }
		  printf("Fehler: recvfrom, fehler code: %d\n", WSAGetLastError());
		}
		break;
	  } else {
		buf[rc] = '\0';
		printf("Nachricht empfangen: %s, auf Socket: %d!\n", buf, mySocket_info.port);

		char *ptr;
		ptr = strtok(buf, "/");

		receivedNeighbourID = atoi(ptr);
		// naechsten Abschnitt erstellen
		ptr = strtok(NULL, "/");
		receivedNeighbourPORT = atoi(ptr);

		ptr = strtok(NULL, "/");
		receivedNeighbourCOST = atoi(ptr);

		printf("receivedNeighbourID: %d\t receivedNeighbourPORT: %d\t receivedNeighbourCOST: %d\n",
			   receivedNeighbourID,
			   receivedNeighbourPORT,
			   receivedNeighbourCOST);

		time(&currentTime);
		int wasInDatabase = 0;
		for (int i = 0; i < 5; i++) {
		  if (mySocket_info.neightbours[i].neightbourId == receivedNeighbourID) {
			if (mySocket_info.neightbours[i].cost > receivedNeighbourCOST || mySocket_info.neightbours[i].cost == 0) {
			  mySocket_info.neightbours[i].lastUpate = currentTime;
			  mySocket_info.neightbours[i].cost = receivedNeighbourCOST;
			  mySocket_info.neightbours[i].port = receivedNeighbourPORT;
			}
			wasInDatabase = 1;
		  } else if (difftime(currentTime, mySocket_info.neightbours[i].lastUpate) >= 30) {
			mySocket_info.neightbours[i].neightbourId = 0;
			mySocket_info.neightbours[i].lastUpate = 0;
			mySocket_info.neightbours[i].port = 0;
			mySocket_info.neightbours[i].cost = 0;
		  }
		}
		if (!wasInDatabase) {
		  for (int i = 0; i < 5; i++) {
			if (mySocket_info.neightbours[i].neightbourId == 0) {
			  mySocket_info.neightbours[i].neightbourId = receivedNeighbourID;
			  mySocket_info.neightbours[i].lastUpate = currentTime;
			  mySocket_info.neightbours[i].port = receivedNeighbourPORT;
			  mySocket_info.neightbours[i].cost = receivedNeighbourCOST;
			  break;
			}
		  }
		}
		printf("IDs der Nachbaren lauten von %d:\n", mySocket_info.id);
		printf("%d,%d,%d,%d,%d\n",
			   mySocket_info.neightbours[0].neightbourId,
			   mySocket_info.neightbours[1].neightbourId,
			   mySocket_info.neightbours[2].neightbourId,
			   mySocket_info.neightbours[3].neightbourId,
			   mySocket_info.neightbours[4].neightbourId);
		printf("\n");

		printf("Cost der Nachbaren lauten von %d:\n", mySocket_info.id);
		printf("%d,%d,%d,%d,%d\n",
			   mySocket_info.neightbours[0].cost,
			   mySocket_info.neightbours[1].cost,
			   mySocket_info.neightbours[2].cost,
			   mySocket_info.neightbours[3].cost,
			   mySocket_info.neightbours[4].cost);
		printf("\n");
		printf("Ports der Nachbaren lauten von %d:\n", mySocket_info.id);
		printf("%d,%d,%d,%d,%d\n",
			   mySocket_info.neightbours[0].port,
			   mySocket_info.neightbours[1].port,
			   mySocket_info.neightbours[2].port,
			   mySocket_info.neightbours[3].port,
			   mySocket_info.neightbours[4].port);
		printf("\n");
	  }
	  recvCounter++;
	}
  }
  printf("Socket %d was terminated.", mySocket_info.port);
  WSACleanup();
  return;
}

void sendOutIDInfo(struct SocketInfo info, SOCKET s) {

  char bufID[300];
  char bufPort[300];
  char bufCost[300];

  char nbufID[300];
  char nbufPort[300];
  char nbufCost[300];
  char nArray[5][300];

  int i = 0;
  int rc;
  SOCKADDR_IN remoteAddr;
  int remoteAddrLen = sizeof(SOCKADDR_IN);
  size_t n = sizeof(info.neightbours) / sizeof(info.neightbours[0]);
  while (i < n) {
	if (info.neightbours[i].neightbourId != 0) {
	  sprintf(bufID, "%d", info.id);
	  sprintf(bufPort, "/%d", info.port);
	  sprintf(bufCost, "/%d", 1);
	  strcat(bufID, bufPort);
	  strcat(bufID, bufCost);

	  for (int i2 = 0; i2 < 5; i2++) {
		if (info.neightbours[i2].neightbourId != 0 && info.neightbours[i].port != info.neightbours[i2].port
			&& info.neightbours[i].cost == 1) {
		  sprintf(nbufID, "%d", info.neightbours[i2].neightbourId);
		  sprintf(nbufPort, "/%d", info.port);
		  sprintf(nbufCost, "/%d", (info.neightbours[i2].cost + 1));
		  strcat(nbufID, nbufPort);
		  strcat(nbufID, nbufCost);
		  strcpy(nArray[i2], nbufID);
		  printf("Das ist der nBufID: %s\n", nbufID);

		  remoteAddr.sin_family = AF_INET;
		  remoteAddr.sin_port = htons(info.neightbours[i].port);
		  remoteAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
		  rc = sendto(s, nbufID, strlen(bufID), 0, (SOCKADDR *)&remoteAddr, remoteAddrLen);

		  if (rc == SOCKET_ERROR) {
			printf("Fehler: sendto, fehler code: %d\n", WSAGetLastError());
			return;
		  } else {
			printf("%s Nachricht gesendet! Von: %d an %d\n",
				   nbufID,
				   info.port,
				   info.neightbours[i].port);
		  }
		}
	  }

	  remoteAddr.sin_family = AF_INET;
	  remoteAddr.sin_port = htons(info.neightbours[i].port);
	  remoteAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	  rc = sendto(s, bufID, strlen(bufID), 0, (SOCKADDR *)&remoteAddr, remoteAddrLen);

	  if (rc == SOCKET_ERROR) {
		printf("Fehler: sendto, fehler code: %d\n", WSAGetLastError());
		return;
	  } else {
		printf("%s Nachricht gesendet! Von: %d an %d\n",
			   bufID,
			   info.port,
			   info.neightbours[i].port);
	  }

	}
	i++;
  }
}
